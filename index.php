<?php
session_start();
$page = "index";
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="style.css" />
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link rel="icon" href="favicon.png" type="image/png" />
		<title>Florian Bratec</title>
	</head>
	<body>
		<img id="logo" src="logo.png"/>
		<div id="bloc_login">
			<div id="bloc_login_center">
				<form class="form" method="post" action="login.php">
				<p>
					<input type="text" name="name" placeholder="identifiant"/><br/>
					<input type="text" name="domain" placeholder="domaine"/><br/>
					<input type="password" name="password" placeholder="mot de passe"/><br/>
					<input type="submit" value="CONNEXION"/>
				</p>
				</form>
			</div>
		</div>
	</body>
</html>
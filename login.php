<?php
session_start();
	
// Connexion à la bdd
include("connexion_bdd.php");
	
// Sécurisation du name et du password envoyés
$name = htmlspecialchars($_POST['name']);
$domain = htmlspecialchars($_POST['domain']);
$password = htmlspecialchars($_POST['password']);

// Vérification du name envoyé
$name_checking = $bdd->query('SELECT name FROM dev1');

$name_check = false;

while($names = $name_checking->fetch()){
	if($names['name'] == $name){ $name_check = true; }
}

$name_checking->closeCursor();

if(isset($domain) AND $domain == 'dev1'){
	if($name_check){
		// Récupération des données
		$reponse = $bdd->prepare('SELECT * FROM dev1 WHERE name = ?');
		$reponse->execute(array($name));
		$donnees = $reponse->fetch();
		
		// Affichage de la page si le mot de passe correspond au name	
		if(isset($donnees['password']) AND $donnees['ban'] == 0){
			if($donnees['active'] == 1){
				if(hash_equals($donnees['password'], crypt($_POST['password'], $donnees['password']))){
					// Création des paramètres de session
					$_SESSION['name'] = $donnees['name'];
					$_SESSION['id'] = $donnees['id'];

					// Enregistrement de la date et de l'horaire de connexion
					$reponse = $bdd->prepare('UPDATE dev1 SET last_connexion = NOW() WHERE name = ?');
					$reponse->execute(array($name));
					
					// Fin de la requête SQL
					$reponse->closeCursor();
					
					header('Location: http://dev1.florianbratec.fr/session_gen.php?name=' . $name . '&password=' . $password);
				}else{
					header('Location: index.php?info=password');
				}	
			}else{
				header('Location: index.php?info=off');
			}
		}else{
			header('Location: index.php?info=refuse');
		}
	}else{
		header('Location: index.php?info=name');
	}
	
}elseif(isset($domain) AND $domain == 'suivi'){
	if($name_check){
		// Récupération des données
		$reponse = $bdd->prepare('SELECT * FROM suivi WHERE name = ?');
		$reponse->execute(array($name));
		$donnees = $reponse->fetch();
		
		// Affichage de la page si le mot de passe correspond au name	
		if(isset($donnees['password']) AND $donnees['ban'] == 0){
			if($donnees['active'] == 1){
				if(hash_equals($donnees['password'], crypt($_POST['password'], $donnees['password']))){
					// Création des paramètres de session
					$_SESSION['name'] = $donnees['name'];
					$_SESSION['id'] = $donnees['id'];

					// Enregistrement de la date et de l'horaire de connexion
					$reponse = $bdd->prepare('UPDATE dev1 SET last_connexion = NOW() WHERE name = ?');
					$reponse->execute(array($name));
					
					// Fin de la requête SQL
					$reponse->closeCursor();
					
					header('Location: http://dev1.florianbratec.fr/session_gen.php?name=' . $name . '&password=' . $password);
				}else{
					header('Location: index.php?info=password');
				}	
			}else{
				header('Location: index.php?info=off');
			}
		}else{
			header('Location: index.php?info=refuse');
		}
	}else{
		header('Location: index.php?info=name');
	}
}else{
	header('Location: index.php?info=no_domain');
}
?>
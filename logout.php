<?php
session_start();

// Destruction des paramètres de session
$_SESSION = array();

// Suppression de la session
session_destroy();

// Redirection à la page de connexion
header('Location: index.php');
?>
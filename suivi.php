<?php
session_start();

if(!isset($_SESSION['suivi']) OR $_SESSION['suivi'] == false){
	header('Location: logout.php');
}

include("connexion_bdd.php");
$page = "suivi";
?>
<!DOCTYPE html>
<html>
	<head>
		<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="style.css" />
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link rel="icon" href="favicon.png" type="image/png" />
		<title>Suivi</title>
	</head>
	<body>
		<div id="bloc_1">
			<?php			
			// Récupération des tâches
			$tasks_select = $bdd->prepare('SELECT * FROM tasks WHERE activation = ? ORDER BY id DESC');
			$tasks_select->execute(array(1));
			
			while($task = $tasks_select->fetch()){
				echo '<div id="task_' . $task['id'] . '" class="task"><h1>' . $task['titre'] . '</h1><br/>';
				
				// Récupération du détail
				$detail_select = $bdd->prepare('SELECT * FROM detail WHERE id_task = ? AND activation = ? ORDER BY id');
				$detail_select->execute(array($task['id'], 1));
				
				while($detail = $detail_select->fetch()){
					// Définition de la classe et de la couleur de la jauge
					if($detail['validation'] AND $detail['paiement']){
						$classe = 'detail_1';
						$bg_color = '130, 175, 5, 1';
						$color = '130, 175, 5, 1';
					}elseif($detail['validation']){
						$classe = 'detail_1';
						$bg_color = '160, 100, 180, 1';
						$color = '160, 100, 180, 1';
					}else{
						$classe = 'detail_0';
						$bg_color = '0, 0, 0, 0.1';
						$color = '0, 0, 0, 0.2';
					}
					
					// Définition de l'unité d'heure
					if($detail['temps'] > 1){ $heure = 'heures'; }else{ $heure = 'heure'; }
					
					// Définition de la longeur de la jauge
					$width = $detail['avancement'] * 2;
					
					// Définition du coût de la ligne
					$cout = $detail['temps'] * 17.5;
					$cout = preg_replace('#\.#', ',', $cout);
					
					// Affichage de la ligne
					echo '
					<div class="' . $classe . '">
						<span class="detail_libelle">' . $detail['libelle'] . '</span>
						<span class="detail_avancement">' . $detail['avancement'] . '%</span>
						<span title="Avancement : ' . $detail['avancement'] . '%" class="detail_jauge"><div style="background-color: rgba(' . $bg_color . '); width: ' . $width . 'px;" class="jauge"></div></span>
						<span title="Temps d\'exécution de la tâche" class="detail_temps">' . $detail['temps'] . ' ' . $heure . '</span>
						<span title="Coût de la tâche" style="color: rgba(' . $color . ');" class="detail_cout">' . $cout . ' €</span>
					</div><br/>';
				}
				
				$detail_select->closeCursor();
				
				echo '</div>';
				
				echo '
				<div id="recap_' . $task['id'] . '" class="recap">
					<h1>' . $task['titre'] . '</h1><br/>';
					
				// Récupération du montant payé
				$detail_select = $bdd->prepare('SELECT SUM(temps) AS total_temps FROM detail WHERE id_task = ? AND validation = ? AND activation = ? AND paiement = ?');
				$detail_select->execute(array($task['id'], 1, 1, 1));
				
				$detail = $detail_select->fetch();
				
				$total_temps_1 = $detail['total_temps'];
				
				$detail_select->closeCursor();
				
				$total_percu = $total_temps_1 * 17.5;
				$total_percu = preg_replace('#\.#', ',', $total_percu);
				
				// Récupération du montant à payer
				$detail_select = $bdd->prepare('SELECT SUM(temps) AS total_temps FROM detail WHERE id_task = ? AND validation = ? AND activation = ? AND paiement = ?');
				$detail_select->execute(array($task['id'], 1, 1, 0));
				
				$detail = $detail_select->fetch();
				
				$total_temps_2 = $detail['total_temps'];
				
				$detail_select->closeCursor();
				
				$total_attendu = $total_temps_2 * 17.5;
				$total_attendu = preg_replace('#\.#', ',', $total_attendu);
				
				// Récupération du montant en attente
				$detail_select = $bdd->prepare('SELECT SUM(temps) AS total_temps FROM detail WHERE id_task = ? AND validation = ? AND activation = ? AND paiement = ?');
				$detail_select->execute(array($task['id'], 0, 1, 0));
				
				$detail = $detail_select->fetch();
				
				$total_temps_3 = $detail['total_temps'];
				
				$detail_select->closeCursor();
				
				$total_engage = $total_temps_3 * 17.5;
				$total_engage = preg_replace('#\.#', ',', $total_engage);
				
				// Affichage
				echo '<h2>Total réglé :</h2><span style="color: rgba(130, 175, 5, 1);" class="montant">' . $total_percu . ' €</span><br/>';
				
				if($total_temps_1 > 1){
					echo '<span class="heures">' . $total_temps_1 . ' heures</span><br/>';
				}elseif($total_temps_1 == 1){
					echo '<span class="heures">1 heure</span><br/>';
				}else{
					echo '<span class="heures">-</span><br/>';
				}
				
				echo '<h2>Total en attente :</h2><span style="color: rgba(160, 100, 180, 1);" class="montant">' . $total_attendu . ' €</span><br/>';
				
				if($total_temps_2 > 1){
					echo '<span class="heures">' . $total_temps_2 . ' heures</span><br/>';
				}elseif($total_temps_2 == 1){
					echo '<span class="heures">1 heure</span><br/>';
				}else{
					echo '<span class="heures">-</span><br/>';
				}
				
				echo '<h2>Reste engagé :</h2><span style="color: rgba(0, 0, 0, 0.2);" class="montant">' . $total_engage . ' €</span><br/>';
				
				if($total_temps_3 > 1){
					echo '<span class="heures">' . $total_temps_3 . ' heures</span><br/>';
				}elseif($total_temps_3 == 1){
					echo '<span class="heures">1 heure</span><br/>';
				}else{
					echo '<span class="heures">-</span><br/>';
				}
				
				echo '<span id="tarif">Tarif horaire : 17,5 € HT<br/>TVA non applicable</span>';
				
				echo '</div>';
				
			}
			
			$tasks_select->closeCursor();
			?>
		</div>
		<div id="bloc_2">
			<?php
			echo '<h1>Paiements</h1><br/>';
			
			// Récupération du montant payé
			$detail_select = $bdd->prepare('SELECT SUM(temps) AS total_temps FROM detail WHERE validation = ? AND activation = ? AND paiement = ?');
			$detail_select->execute(array(1, 1, 1));
			
			$detail = $detail_select->fetch();
			
			$total_temps_1 = $detail['total_temps'];
			
			$detail_select->closeCursor();
			
			$total_percu = $total_temps_1 * 17.5;
			$total_percu = preg_replace('#\.#', ',', $total_percu);
			
			// Récupération du montant à payer
			$detail_select = $bdd->prepare('SELECT SUM(temps) AS total_temps FROM detail WHERE validation = ? AND activation = ? AND paiement = ?');
			$detail_select->execute(array(1, 1, 0));
			
			$detail = $detail_select->fetch();
			
			$total_temps_2 = $detail['total_temps'];
			
			$detail_select->closeCursor();
			
			$total_attendu = $total_temps_2 * 17.5;
			$total_attendu = preg_replace('#\.#', ',', $total_attendu);
			
			// Récupération du montant en attente
			$detail_select = $bdd->prepare('SELECT SUM(temps) AS total_temps FROM detail WHERE validation = ? AND activation = ? AND paiement = ?');
			$detail_select->execute(array(0, 1, 0));
			
			$detail = $detail_select->fetch();
			
			$total_temps_3 = $detail['total_temps'];
			
			$detail_select->closeCursor();
			
			$total_engage = $total_temps_3 * 17.5;
			$total_engage = preg_replace('#\.#', ',', $total_engage);
			
			// Affichage
			echo '<h2>Total réglé :</h2><span style="color: rgba(130, 175, 5, 1);" class="montant">' . $total_percu . ' €</span><br/>';
			
			if($total_temps_1 > 1){
				echo '<span class="heures">' . $total_temps_1 . ' heures</span><br/>';
			}elseif($total_temps_1 == 1){
				echo '<span class="heures">1 heure</span><br/>';
			}else{
				echo '<span class="heures">-</span><br/>';
			}
			
			echo '<h2>Total en attente :</h2><span style="color: rgba(160, 100, 180, 1);" class="montant">' . $total_attendu . ' €</span><br/>';
			
			if($total_temps_2 > 1){
				echo '<span class="heures">' . $total_temps_2 . ' heures</span><br/>';
			}elseif($total_temps_2 == 1){
				echo '<span class="heures">1 heure</span><br/>';
			}else{
				echo '<span class="heures">-</span><br/>';
			}
			
			echo '<h2>Reste engagé :</h2><span style="color: rgba(0, 0, 0, 0.2);" class="montant">' . $total_engage . ' €</span><br/>';
			
			if($total_temps_3 > 1){
				echo '<span class="heures">' . $total_temps_3 . ' heures</span><br/>';
			}elseif($total_temps_3 == 1){
				echo '<span class="heures">1 heure</span><br/>';
			}else{
				echo '<span class="heures">-</span><br/>';
			}
			
			echo '<span id="tarif">Tarif horaire : 17,5 € HT<br/>TVA non applicable</span>';
			?>
		</div>
	</body>
</html>